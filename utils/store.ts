import { Store } from 'vuex';
import { getModule } from 'vuex-module-decorators';
import AuthStore from '~/store/auth';
// eslint-disable-next-line
let authStore: AuthStore;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function initialiseStores(store: Store<any>): void {
  authStore = getModule(AuthStore, store);
}

export { initialiseStores, authStore };
