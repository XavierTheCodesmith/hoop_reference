import {Module, VuexModule, Mutation, Action} from 'vuex-module-decorators';

@Module({namespaced: true, stateFactory: true, name: 'auth'})
export default class Auth extends VuexModule {
	loggedIn: boolean = false;
	currentUser: string | null = null;

	public get getAuthState(): {
		loggedIn: boolean;
		currentUser: string | null;
	} {
		return {loggedIn: this.loggedIn, currentUser: this.currentUser};
  }

	@Mutation
	loginMutation(user: string) {
		this.loggedIn = true;
    this.currentUser = user;
	}

	@Mutation
	public setLogout() {
		this.loggedIn = false;
		this.currentUser = null;
	}

	@Action
	public setUser({ authUser }: {authUser: firebase.User}) {
		this.context.commit('loginMutation', authUser.email);
	}
}
