import { Store } from 'vuex';
import { initialiseStores } from '~/utils/store';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const initializer = (store: Store<any>): void => initialiseStores(store);

export const plugins = [initializer];
export * from '~/utils/store';
