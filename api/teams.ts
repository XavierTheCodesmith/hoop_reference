import nba from 'nba-api-client';

export const getTeamDetail = async (teamID: number) => {
  return await nba.teamDetails({ TeamID: teamID });
};
