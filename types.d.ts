declare module 'nba-api-client' {
  export function teamDetails({ TeamID }: { TeamID: number }): Promise<any>;
}
